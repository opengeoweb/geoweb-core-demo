/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */

import * as React from 'react';
import {
  mapActions,
  MapViewConnect,
  MultiDimensionSelectConnect,
  TimeSliderConnect,
  LegendConnect,
  LayerManagerButton,
} from '@opengeoweb/core';
import { store } from '@opengeoweb/core/store';
import { GWTheme } from '@opengeoweb/theme';
import { ThemeProvider } from '@material-ui/core';
import { Provider, connect } from 'react-redux';

import { getPresets } from '../utils/JsonPresetFilter';

const connectRedux = connect(null, {
  setLayers: mapActions.setLayers,
  setBaseLayers: mapActions.setBaseLayers,
  setBbox: mapActions.setBbox,
});

interface MapWithGeoWebLayerSelectProps {
  setLayers: typeof mapActions.setLayers;
  setBaseLayers: typeof mapActions.setBaseLayers;
  setBbox: typeof mapActions.setBbox;
}

const MapWithGeoWebLayerSelect: React.FC = connectRedux(
  ({ setLayers, setBaseLayers, setBbox }: MapWithGeoWebLayerSelectProps) => {
    const mapId = 'mapid_1';
    const {
      bbox,
      services,
      srs,
      initialBaseLayers,
      baseLayers,
      mapLayers,
    } = getPresets();
    React.useEffect(() => {
      setLayers({
        layers: mapLayers,
        mapId,
      });
      setBaseLayers({
        layers: initialBaseLayers,
        mapId,
      });
      setBbox({
        bbox,
        srs,
        mapId,
      });
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
      <ThemeProvider theme={GWTheme}>
        <Provider store={store}>
          <div style={{ height: '100%', width: '100%', position: 'relative' }}>
            <MultiDimensionSelectConnect mapId={mapId} />
            <LegendConnect mapId={mapId} />
            <MapViewConnect mapId={mapId} displayTimeInMap />
            <div
              style={{
                position: 'absolute',
                left: '0px',
                bottom: '0px',
                zIndex: 1000,
                width: '100%',
              }}
            >
              <TimeSliderConnect mapId={mapId} />
            </div>
            <div
              style={{
                position: 'absolute',
                left: '0px',
                top: '10px',
                zIndex: 1000,
              }}
            >
              <LayerManagerButton
                mapId={mapId}
                preloadedAvailableBaseLayers={baseLayers}
                preloadedBaseServices={services}
                preloadedMapServices={services}
              />
            </div>
          </div>
        </Provider>
      </ThemeProvider>
    );
  },
);

const ConnectedMapWithGeoWebLayerSelect: React.FC = () => (
  <Provider store={store}>
    <MapWithGeoWebLayerSelect />
  </Provider>
);

export default ConnectedMapWithGeoWebLayerSelect;
