/* *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import { mapTypes } from '@opengeoweb/core';
import { Service } from './JsonPresetFilter';

export const defaultSrs = 'EPSG:3857';

export const defaultBbox: mapTypes.Bbox = {
  left: 58703.6377,
  bottom: 6408480.4514,
  right: 3967387.5161,
  top: 11520588.9031,
};

export const defaultBaseLayer: mapTypes.Layer = {
  id: 'base-layer-1',
  name: 'WorldMap_Light_Grey_Canvas',
  type: 'twms',
  layerType: mapTypes.LayerType.baseLayer,
  enabled: true,
};

export const defaultMapLayers: mapTypes.Layer[] = [
  {
    id: 'map-layer-1',
    service: 'https://geoservices.knmi.nl/wms?dataset=RADAR&',
    name: 'RAD_NL25_PCP_CM',
    format: 'image/png',
    enabled: true,
    style: 'knmiradar/nearest',
    layerType: mapTypes.LayerType.mapLayer,
  },
];

export const defaultOverLayers: mapTypes.Layer[] = [
  {
    id: 'over-layer-1',
    service: 'https://geoservices.knmi.nl/wms?DATASET=baselayers&',
    name: 'countryborders',
    format: 'image/png',
    enabled: true,
    layerType: mapTypes.LayerType.overLayer,
  },
];

export const defaultBaseLayers: mapTypes.Layer[] = [
  defaultBaseLayer,
  ...defaultOverLayers,
];

export const defaultServices: Service[] = [
  {
    id: 'default-1',
    name: 'KNMI Radar',
    url: 'https://geoservices.knmi.nl/adagucserver?dataset=RADAR&',
  },
];
