/* *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import { mapTypes } from '@opengeoweb/core';
import { filterPresets } from './JsonPresetFilter';
import {
  defaultBaseLayers,
  defaultBbox,
  defaultMapLayers,
  defaultSrs,
  defaultServices,
} from './defaultPresets';

const emptyPreset = {
  preset: {},
};

const defaultPresets = {
  bbox: defaultBbox,
  services: defaultServices,
  srs: defaultSrs,
  initialBaseLayers: defaultBaseLayers,
  baseLayers: defaultBaseLayers,
  mapLayers: defaultMapLayers,
};

const customPreset = {
  preset: {
    presetType: 'mapPreset',
    srs: 'EPSG:3857',
    bbox: {
      left: 58703.6377,
      bottom: 6408480.4514,
      right: 3967387.5161,
      top: 11520588.9031,
    },
    services: [
      {
        name: 'KNMI Radar',
        url:
          'https://geoservices.knmi.nl/cgi-bin/RADNL_OPER_R___25PCPRR_L3.cgi?',
        id: 'knmi-radar',
      },
      {
        name: 'FMI',
        url: 'https://openwms.fmi.fi/geoserver/wms?',
        id: 'fmi',
      },
      {
        name: 'MET Norway',
        url: 'https://public-wms.met.no/verportal/verportal.map?',
        id: 'met-no',
      },
      {
        name: 'DWD',
        url: 'https://maps.dwd.de/geoserver/ows?',
        id: 'dwd',
      },
      {
        name: 'EUMETSAT',
        url: 'https://eumetview.eumetsat.int/geoserver/wms?',
        id: 'eumetsat',
      },
    ],
    layers: [
      {
        id: 'base-layer-1',
        name: 'WorldMap_Light_Grey_Canvas',
        type: 'twms',
        layerType: mapTypes.LayerType.baseLayer,
      },
      {
        id: 'base-layer-2',
        name: 'OpenStreetMap_NL',
        type: 'twms',
        layerType: mapTypes.LayerType.baseLayer,
        enabled: 'true',
      },
      {
        id: 'base-layer-3',
        service:
          '//services.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/',
        name: 'arcGisCanvas',
        type: 'twms',
        layerType: mapTypes.LayerType.baseLayer,
        enabled: 'true',
      },
      {
        service: '//geoservices.knmi.nl/cgi-bin/worldmaps.cgi?',
        name: 'ne_10m_admin_0_countries_simplified',
        format: 'image/png',
        enabled: 'true',
        layerType: mapTypes.LayerType.overLayer,
      },
      {
        service: '//geoservices.knmi.nl/cgi-bin/RADNL_OPER_R___25PCPRR_L3.cgi?',
        name: 'RADNL_OPER_R___25PCPRR_L3_COLOR',
        format: 'image/png',
        enabled: 'true',
        style: 'knmiradar/nearest',
        layerType: mapTypes.LayerType.mapLayer,
      },
      {
        service: '//openwms.fmi.fi/geoserver/wms?',
        name: 'Radar:suomi_dbz_eureffin',
        format: 'image/png',
        enabled: 'true',
        style: 'Radar dbz Summer',
        layerType: mapTypes.LayerType.mapLayer,
      },
      {
        service: '//public-wms.met.no/verportal/verportal.map?',
        name: 'radar_precipitation_intensity_nordic',
        format: 'image/png',
        enabled: 'true',
        style: '',
        layerType: mapTypes.LayerType.mapLayer,
      },
    ],
  },
};

const filteredCustomPresets = {
  baseLayers: [
    {
      id: 'base-layer-1',
      layerType: 'baseLayer',
      name: 'WorldMap_Light_Grey_Canvas',
      type: 'twms',
    },
    {
      enabled: true,
      id: 'base-layer-2',
      layerType: 'baseLayer',
      name: 'OpenStreetMap_NL',
      type: 'twms',
    },
    {
      enabled: true,
      id: 'base-layer-3',
      layerType: 'baseLayer',
      name: 'arcGisCanvas',
      service:
        '//services.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/',
      type: 'twms',
    },
  ],
  bbox: {
    bottom: 6408480.4514,
    left: 58703.6377,
    right: 3967387.5161,
    top: 11520588.9031,
  },
  initialBaseLayers: [
    {
      id: 'base-layer-1',
      layerType: 'baseLayer',
      name: 'WorldMap_Light_Grey_Canvas',
      type: 'twms',
    },
    {
      enabled: true,
      format: 'image/png',
      id: 'layerid_1',
      layerType: 'overLayer',
      name: 'ne_10m_admin_0_countries_simplified',
      service: '//geoservices.knmi.nl/cgi-bin/worldmaps.cgi?',
    },
  ],
  mapLayers: [
    {
      enabled: true,
      format: 'image/png',
      id: 'layerid_2',
      layerType: 'mapLayer',
      name: 'RADNL_OPER_R___25PCPRR_L3_COLOR',
      service: '//geoservices.knmi.nl/cgi-bin/RADNL_OPER_R___25PCPRR_L3.cgi?',
      style: 'knmiradar/nearest',
    },
    {
      enabled: true,
      format: 'image/png',
      id: 'layerid_3',
      layerType: 'mapLayer',
      name: 'Radar:suomi_dbz_eureffin',
      service: '//openwms.fmi.fi/geoserver/wms?',
      style: 'Radar dbz Summer',
    },
    {
      enabled: true,
      format: 'image/png',
      id: 'layerid_4',
      layerType: 'mapLayer',
      name: 'radar_precipitation_intensity_nordic',
      service: '//public-wms.met.no/verportal/verportal.map?',
      style: '',
    },
  ],
  services: [
    {
      id: 'knmi-radar',
      name: 'KNMI Radar',
      url: 'https://geoservices.knmi.nl/cgi-bin/RADNL_OPER_R___25PCPRR_L3.cgi?',
    },
    { id: 'fmi', name: 'FMI', url: 'https://openwms.fmi.fi/geoserver/wms?' },
    {
      id: 'met-no',
      name: 'MET Norway',
      url: 'https://public-wms.met.no/verportal/verportal.map?',
    },
    { id: 'dwd', name: 'DWD', url: 'https://maps.dwd.de/geoserver/ows?' },
    {
      id: 'eumetsat',
      name: 'EUMETSAT',
      url: 'https://eumetview.eumetsat.int/geoserver/wms?',
    },
  ],
  srs: 'EPSG:3857',
};

describe('utils/filterPresets', () => {
  it('should return default values if presets is empty', () => {
    expect(filterPresets(emptyPreset)).toEqual(defaultPresets);
  });

  it('should return filtered presets', () => {
    expect(filterPresets(customPreset)).toEqual(filteredCustomPresets);
  });
});
