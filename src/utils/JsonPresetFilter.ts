/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */

import { generateLayerId, mapTypes } from '@opengeoweb/core';
import Presets from './Presets.json';
import {
  defaultBaseLayer,
  defaultBaseLayers,
  defaultBbox,
  defaultMapLayers,
  defaultOverLayers,
  defaultServices,
  defaultSrs,
} from './defaultPresets';

export interface JsonPresets {
  preset: Preset;
}

export interface Preset {
  presetType?: string;
  srs?: string;
  bbox?: mapTypes.Bbox;
  services?: Service[];
  layers?: PresetLayer[];
}

export interface Service {
  id: string;
  name: string;
  url: string;
}

export interface PresetLayer {
  id?: string;
  mapId?: string;
  service?: string;
  name?: string;
  format?: string;
  enabled?: string;
  style?: string;
  opacity?: number;
  type?: string;
  layerType?: string;
  status?: string;
}

export interface FilteredPresets {
  bbox: mapTypes.Bbox;
  services: Service[];
  srs: string;
  initialBaseLayers: mapTypes.Layer[];
  baseLayers: mapTypes.Layer[];
  mapLayers: mapTypes.Layer[];
}

const parseBoolean = (value): boolean => {
  return ['true', 'false', true, false].includes(value) && JSON.parse(value);
};

const isBaseLayer = (layer): boolean => {
  return ['baseLayer'].includes(layer.layerType);
};

const isMapLayer = (layer): boolean => {
  return ['mapLayer'].includes(layer.layerType);
};

const isOverLayer = (layer): boolean => {
  return ['overLayer'].includes(layer.layerType);
};

const isValidService = (service): boolean => {
  return Object.prototype.hasOwnProperty.call(service, 'url');
};

const isValidBbox = (bbox): boolean => {
  const isFiniteNumber =
    Number.isFinite(bbox.bottom) &&
    Number.isFinite(bbox.left) &&
    Number.isFinite(bbox.right) &&
    Number.isFinite(bbox.top);

  return isFiniteNumber;
};

const isValidSrs = (srs): boolean => {
  const isValid = srs.includes('EPSG');
  return isValid;
};

const parseLayerType = (layer, defaultValue): unknown => {
  if (isBaseLayer(layer)) {
    return mapTypes.LayerType.baseLayer;
  }

  if (isMapLayer(layer)) {
    return mapTypes.LayerType.mapLayer;
  }

  if (isOverLayer(layer)) {
    return mapTypes.LayerType.overLayer;
  }

  return defaultValue;
};

const parseLayer = (layer): mapTypes.Layer => {
  const lr = JSON.parse(JSON.stringify(layer)); // Cloning used here to achieve object immutability
  if (!lr.id) {
    lr.id = generateLayerId();
  }

  if (lr.layerType) {
    lr.layerType = parseLayerType(lr, null);
  }

  if (lr.enabled) {
    lr.enabled = parseBoolean(lr.enabled);
  }

  return lr;
};

export const filterBaseLayers = (presetsArray: unknown[]): mapTypes.Layer[] => {
  const hasLayers =
    presetsArray && Array.isArray(presetsArray) && presetsArray.length > 0;
  const filteredBaseLayers = hasLayers
    ? presetsArray
        .filter(preset => isBaseLayer(preset))
        .map(filteredPresetLayer => parseLayer(filteredPresetLayer))
    : null;
  const hasBaseLayers = filteredBaseLayers && filteredBaseLayers.length > 0;
  const baseLayers = hasBaseLayers ? filteredBaseLayers : defaultBaseLayers;

  return baseLayers;
};

export const filterInitialBaseLayer = (
  presetsArray: unknown[],
): mapTypes.Layer => {
  const baseLayers = filterBaseLayers(presetsArray);
  const InitialBaseLayer =
    baseLayers && baseLayers[0] ? baseLayers[0] : defaultBaseLayer;

  return InitialBaseLayer;
};

export const filterMapLayers = (presetsArray: unknown[]): mapTypes.Layer[] => {
  const hasLayers =
    presetsArray && Array.isArray(presetsArray) && presetsArray.length > 0;
  const filteredMapLayers = hasLayers
    ? presetsArray
        .filter(preset => isMapLayer(preset))
        .map(filteredPresetLayer => parseLayer(filteredPresetLayer))
    : null;
  const hasMapLayers = filteredMapLayers && filteredMapLayers.length > 0;
  const mapLayers = hasMapLayers ? filteredMapLayers : defaultMapLayers;

  return mapLayers;
};

export const filterOverLayers = (presetsArray: unknown[]): mapTypes.Layer[] => {
  const hasLayers =
    presetsArray && Array.isArray(presetsArray) && presetsArray.length > 0;
  const filteredOverLayers = hasLayers
    ? presetsArray
        .filter(preset => isOverLayer(preset))
        .map(filteredPresetLayer => parseLayer(filteredPresetLayer))
    : null;
  const hasOverLayers = filteredOverLayers && filteredOverLayers.length > 0;
  const overLayers = hasOverLayers ? filteredOverLayers : defaultOverLayers;

  return overLayers;
};

export const filterServices = (presetsArray: Service[]): Service[] => {
  const hasServices =
    presetsArray && Array.isArray(presetsArray) && presetsArray.length > 0;
  const validServices = hasServices
    ? presetsArray.filter(preset => isValidService(preset))
    : null;
  const hasValidServices = validServices && validServices.length > 0;
  const services = hasValidServices ? validServices : defaultServices;

  return services;
};

export const filterBbox = (presetsBbox: mapTypes.Bbox): mapTypes.Bbox => {
  const hasAllValues =
    presetsBbox &&
    presetsBbox.bottom &&
    presetsBbox.left &&
    presetsBbox.right &&
    presetsBbox.top;
  const bbox =
    hasAllValues && isValidBbox(presetsBbox) ? presetsBbox : defaultBbox;

  return bbox;
};

export const filterSrs = (presetsSrs: string): string => {
  const isValid = presetsSrs && isValidSrs(presetsSrs);
  const srs = isValid ? presetsSrs : defaultSrs;

  return srs;
};

export const filterPresets = (presets: JsonPresets): FilteredPresets => {
  const hasPresets = presets && presets.preset;
  if (hasPresets) {
    const { bbox, layers, services, srs } = presets.preset;
    const filteredBbox = bbox ? filterBbox(bbox) : defaultBbox;
    const filteredSrs = srs ? filterSrs(srs) : defaultSrs;
    const filterredServices = services
      ? filterServices(services)
      : defaultServices;
    const initialBaseLayer = layers
      ? filterInitialBaseLayer(layers)
      : defaultBaseLayer;
    const overLayers = layers ? filterOverLayers(layers) : defaultOverLayers;
    const filteredInitialBaseLayers = [initialBaseLayer, ...overLayers];
    const filteredBaseLayers = layers
      ? filterBaseLayers(layers)
      : defaultBaseLayers;
    const filteredMapLayers = layers
      ? filterMapLayers(layers)
      : defaultMapLayers;
    const filteredPreset = {
      bbox: filteredBbox,
      services: filterredServices,
      srs: filteredSrs,
      initialBaseLayers: filteredInitialBaseLayers,
      baseLayers: filteredBaseLayers,
      mapLayers: filteredMapLayers,
    };

    return filteredPreset;
  }
  const defaultPresets = {
    bbox: defaultBbox,
    services: defaultServices,
    srs: defaultSrs,
    initialBaseLayers: defaultBaseLayers,
    baseLayers: defaultBaseLayers,
    mapLayers: defaultMapLayers,
  };

  return defaultPresets;
};

/**
 * Gets filtered presets.
 *
 * Example: getPresets();
 * @returns {FilteredPresets} returnType: FilteredPresets
 */
export const getPresets = (): FilteredPresets => filterPresets(Presets);
