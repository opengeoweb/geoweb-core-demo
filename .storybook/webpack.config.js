/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */

const path = require('path');
const SRC_PATH = path.join(__dirname, '../src');
const STORIES_PATH = path.join(__dirname, '../stories');
const CopyWebpackPlugin = require('copy-webpack-plugin');
//dont need stories path if you have your stories inside your //components folder
module.exports = ({ config }) => {
  config.plugins.push(new CopyWebpackPlugin([{ from: 'src/assets' }]));
  config.module.rules.push({
    test: /\.(ts|tsx)$/,
    include: [SRC_PATH, STORIES_PATH],
    use: [
      {
        loader: require.resolve('awesome-typescript-loader'),
        options: {
          configFileName: './.storybook/tsconfig.json',
        },
      },
      { loader: require.resolve('react-docgen-typescript-loader') },
    ],
  });
  config.module.rules.push({
    test: /\.(scss)$/,
    include: [SRC_PATH, STORIES_PATH],
    use: [
      {
        loader: 'style-loader', // inject CSS to page
      },
      {
        loader: 'css-loader', // translates CSS into CommonJS modules
      },
      {
        loader: 'postcss-loader', // Run post css actions
        options: {
          plugins: function() {
            // post css plugins, can be exported to postcss.config.js
            return [require('precss'), require('autoprefixer')];
          },
        },
      },
      {
        loader: 'sass-loader', // compiles Sass to CSS
      },
    ],
  });
  config.resolve.extensions.push('.ts', '.tsx');
  (config.resolve.alias = {
    react: path.resolve('./node_modules/react'),
  }),
    config.module.rules.push({
      test: /\.stories\.jsx?$/,
      loaders: [
        {
          loader: require.resolve('@storybook/addon-storysource/loader'),
          options: { parser: 'typescript' },
        },
      ],
      enforce: 'pre',
    });

  return config;
};
