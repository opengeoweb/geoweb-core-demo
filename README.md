# GeoWeb Reference Application

This project is intended as an example on how to use the GeoWeb library to build a custom application.

For more information, please refer to the the [geoweb-core NPM page](https://www.npmjs.com/package/@opengeoweb/core) and its [GitLab repo](https://gitlab.com/opengeoweb/geoweb-core/).

GeoWeb Reference Application [website](https://demo.opengeoweb.com).

### Installation

To run the geoweb-application storybook or application you need npm. The easiest way to install npm is via nvm. Please visit https://nvm.sh/ and follow the instructions. When nvm is installed, please run the following command:

```
nvm install 8
```

### To run the storybook

```
npm ci
npm run storybook
```

### To run the application

```
npm ci
npm run start
```

### To build the application and serve the dist folder locally

```
npm ci
npm run build
npx serve dist
```

### Managing packages

Follow the instructions below for dependency management. Please refer to [the documentation](https://docs.npmjs.com/cli-documentation/) for more information on any of the specific commands.

#### Removing extraneous packages

During development, we may end up with packages that have been installed but are not actually being used. In order to remove them, run `npm prune`.

#### Adding a new dependency

Adding a new dependency can be done following these steps:

1. `npm install <package-name>` or `npm install <package-name> --save-dev`
2. commit the updated files package.json and package-lock.json

Make sure to add the package as a dev depencency when it's only used for development purposes (like storybook or linting).

#### Updating an existing dependency

To see a list of which packages could need updating, run `npm outdated -l`.
A red package name means there’s a newer version matching the semver requirements in the package.json, so it can be updated. Yellow indicates that there’s a newer version higher than the semver requirements in the package.json (usually a new major version), so proceed with caution.

To update a single package to its latest version that matches the version required in package.json (red in the list), run `npm update <package-name>`, and commit the updated package-lock.json file.

To update all packages at once (that are red in the list), run `npm update`, and commit the updated package-lock.json file.

To update a single package to a version higher than the required version in package.json (yellow in the list):

1. check if there are any breaking changes to be aware of in the new version
2. `npm install <package-name>@<version>` or `npm install <package-name>@<version> --save-dev`
3. commit the updated files package.json and package-lock.json
4. update the code related to breaking changes

After any version change, make sure to test if everything still works.

#### Checking for vulnerabilities

To scan the project for any known vulnerabilities in existing dependencies, you can run `npm audit`. To fix them:

1. `npm audit fix`
2. commit the updated package-lock.json
3. test if everything still works

### Branches

About the different branches of this project.

#### Master

The master branch is the main branch and it contains application code that is compatible with the GeoWeb Core dependency version specified in the [package.json](package.json). The version of GeoWeb Core used in this application will be updated when a new version of GeoWeb Core is released and available in [NPM](https://www.npmjs.com/package/@opengeoweb/core).

#### Staging

The Staging branch is used to review the latest changes and additions to the GeoWeb Core dependency [master branch](https://gitlab.com/opengeoweb/geoweb-core/-/tree/master) that have not yet been released. It contains all the necessary changes to the application when using the latest code of the GeoWeb Core.

### Environments and deployments

Information about the environments used in this project.

#### Demo environment

The demo environment is deployed manually. See the steps below.

1. If new version of GeoWeb Core was released, update GeoWeb Core dependency version specified in the [package.json](package.json)
2. Create a tag from the master branch that starts with v*, for example [v0.0.34](https://gitlab.com/opengeoweb/geoweb-core-demo/-/tags/v0.0.34). Tags that start with v* are protected by default in this project.

The pipeline builds the reference app from the code in master branch and NPM package version of the GeoWeb Core specified in the package.json and deploys it to S3 bucket in AWS (https://demo.opengeoweb.com/)

#### Staging environment

The staging environment is deployed automatically when code is merged into the master branch of the GeoWeb Core project. Merging to the master branch in the GeoWeb Core project triggers the pipeline in this project. The pipeline builds the reference app using the code in staging branch and the master branch of GeoWeb Core project as a dependency and deploys the reference app to the staging S3 bucket in AWS (https://staging.opengeoweb.com/)

### License

This project is licensed under the terms of the [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0) license.
You may not use this file except in compliance with the License.
Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.

Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
